import {merkenLijst} from "/merkenlijst.js";
import {prijsLijst} from "/prijslijst.js";
var zoekTerm = "";
var aantalSterren = "";
var gefilterdePrijslijst = [];
var geenResultaatObject = { 2026: "Merk niet gevonden!", "****": "Merk niet gevonden!", "kledij/vêtements": "kledij/vêtements", deborah: "merk E5 mode", "": "deborah" }

//Clear search bar on refresh
document.getElementById("searchTxt").value = "";
function merkOpzoeking(value) {
	//We doorlopen de hele array 
	//2026 is de keynaam van de feitelijke merknaam. Deze naar lowercase en vergelijken met lowercase zoekterm.
	if (value[2026].toString().toLowerCase().indexOf(zoekTerm) !== -1) {
		return value;
	} else {

	}
}

function filterOpzoeking(zoekString)
{
	zoekTerm = zoekString.trim().toLowerCase();
	// We doorlopen de hele merkenlijst, en vergelijken (in de merkOpzoekingsfunctie) met de zoekopdracht. De resultaten die gedeeltelijk overeenkomen
	//Worden in een nieuwe array "filteredSearch" gestoken.
	var filteredSearch = merkenLijst.filter(merkOpzoeking);
	return filteredSearch;
}

//keyhandlers & eventlisteners (Zoekknop, Enter in textvak voor te zoeken)
document.getElementById("zoeken").onclick = function() {zoeken();};
document.getElementById('searchTxt').addEventListener('keypress', zoekKnop);

function zoekKnop(event){
	if(event.keyCode == 13)
	{
		zoeken();
	    return false; // returning false will prevent the event from bubbling up.
	} else {return false;}
}

function zoeken(){
	//Just in case, get previous results and remove them
	verwijderOpzoeking();
	//clear price table
	verwijderPrijslijst()
	var resultaat = filterOpzoeking(document.getElementById("searchTxt").value);
	//Resultaten weergeven
	toonZoekopdracht(resultaat);
}

function verwijderOpzoeking() {
	var zoekResultaten = document.getElementsByClassName("resultRow");
	for (let i = zoekResultaten.length - 1; i >= 0; i--) {
		zoekResultaten[i].parentNode.removeChild(zoekResultaten[i]);
	}
}
function verwijderPrijslijst(){
	var priceNode = document.getElementsByClassName("priceRow");
	for (let i = priceNode.length - 1; i >= 0; i--) {
		priceNode[i].parentNode.removeChild(priceNode[i]);
	}
	var priceNode2 = document.getElementsByClassName("priceHeaderRow");
	for (let i = priceNode2.length - 1; i >= 0; i--) {
		priceNode2[i].parentNode.removeChild(priceNode2[i]);
	}
}
function toonZoekopdracht(results){
	if (!results[0]){
		maakZoekresultaatLijst(geenResultaatObject[2026], geenResultaatObject['****']);
	} else {
	results.forEach(function(result){
		maakZoekresultaatLijst(result[2026], result['****']);
	});}
}

function maakZoekresultaatLijst(text, sterren){
	var zoekrijNode = document.createElement("tr");
	//Listener toevoegen voor Prijslijst na klik.
	//Als het merk in de lijst staat, eventlistener voor prijslijst te openen toevoegen
	if (text !== "Merk niet gevonden!"){
	zoekrijNode.addEventListener("click", prijslijst);
	}
	zoekrijNode.setAttribute("class", "resultRow");
	//merknaamcel
	 var merknaamNode = document.createElement("td");
	 //Sterrencel
	 var sterrenNode = document.createElement("td");
	 //Prijsopzoekingscel
	 var prijsOpzoekingsNode = document.createElement("td");
	// Create Text

	var merknaamText = document.createTextNode(text);
	var sterrenText;
	//In de merkenlijst staat er soms andere data ipv een aantal sterren (bv. Deborah, kledij/vêtements, ...) 
	//Als dit het geval is, vervangen we deze tekst door "onbekend"
	if(sterren.length < 4){
 		 sterrenText = document.createTextNode(sterren);  
 	}  else {
		 sterrenText = document.createTextNode('Onbekend');
	}
	//Als het merk in de lijst staat, de prijsopzoekcel de juiste text geven, en de juiste css klasses.
	if (text !== "Merk niet gevonden!")
	{
		var prijsOpzoekingsText = document.createTextNode("Prijs opzoeken");
		prijsOpzoekingsNode.setAttribute("class", "opzoeken");
	} 
	//Anders vervangen we de prijsopzoekcel door een boodschap dat deze niet mogelijk is.
	else 
	{
		var prijsOpzoekingsText = document.createTextNode("Prijslijst niet mogelijk!");
	}
	//Append  Text to td
	merknaamNode.appendChild(merknaamText);
	prijsOpzoekingsNode.appendChild(prijsOpzoekingsText);
	sterrenNode.appendChild(sterrenText);
	//append td to tr
	zoekrijNode.appendChild(merknaamNode);
	zoekrijNode.appendChild(sterrenNode);
	zoekrijNode.appendChild(prijsOpzoekingsNode);
	//append tr to table.
	document.getElementById("resultTable").appendChild(zoekrijNode);
}



//prijslijst tonen!
function prijslijst(){
	//Als de prijslijst al toegevoegd/uitgeklapt is, verwijderen we deze voor we verdergaan.
	if (document.getElementsByClassName("priceRow").length > 0) {
		verwijderPrijslijst();
	//Anders zoeken we het aantal sterren op, en voegen we de gevraagde prijslijst toe.
	} else {
		//rowindex bepalen om vast te leggen op welke rij we moeten beginnen met onze prijstabel in te voegen.
	var rijIndex = this.rowIndex + 1;
	//Aantal sterren!
	//Als er geen sterren zijn voor een merk, is de data "Onbekend" (lengte van dit woord is langer als 4). 
	//Als deze case afgehandeld is, kunnen we voor de rest .length gebruiken om aan de hand van het aantal sterren
	//de juiste lijst aan te maken en door te geven.
	var tijdelijkePrijslijst;
	if (this.children[1].childNodes[0].data.length > 4){
	} else {
		 tijdelijkePrijslijst = sterrenFilter(this.children[1].childNodes[0].data.length);
	}
	prijsToevoegen(tijdelijkePrijslijst, rijIndex);
}}

function filterPrijslijst(value) {

	var sterren = "*";
	if (aantalSterren === 1) {
	}
	if (aantalSterren === 2){
		sterren = "**";
	} else if (aantalSterren > 2 && aantalSterren < 5){
		sterren = "***";
	}
	var prijslijstOnderdeel = [];
	prijslijstOnderdeel.push(value["Kleding"]);
	prijslijstOnderdeel.push(value[sterren + "Min"]);
	prijslijstOnderdeel.push(value[sterren + "Max"]);
	gefilterdePrijslijst.push(prijslijstOnderdeel);
}

function sterrenFilter(sterren)
{
	//Het is mogelijk dat er al data staat in de gefilterde lijst. 
	if (gefilterdePrijslijst.length > 0){
		gefilterdePrijslijst = [];
	}
	aantalSterren = sterren;
	prijsLijst.filter(filterPrijslijst);
	return gefilterdePrijslijst;
}

function prijsToevoegen(tijdelijkePrijslijst, rijIndex){
	var tabel = document.getElementById("resultTable");
	//Als een merk sterren heeft in de tabel, zit er data in de prijslijst (List)
	if (tijdelijkePrijslijst){
	tijdelijkePrijslijst.forEach(function(element, index) {
		//create table row
		var prijsRij = tabel.insertRow(rijIndex + index);	
		prijsRij.setAttribute("class", "priceRow");
		if (element[1] === "Einde Prijslijst"){
			prijsRij.className += " lastPriceRow";
		}
		//create table cells for data
		var kledingTypeNode = document.createElement("td");
		var minPrijsNode = document.createElement("td");
		var maxPrijsNode = document.createElement("td");
		//create text for nodes
		var kledingTypeText = document.createTextNode(element[0]);
		var minPrijsText = document.createTextNode(element[1]);
		var maxPrijsText = document.createTextNode(element[2]);
		//append text to cells
		kledingTypeNode.appendChild(kledingTypeText);
		minPrijsNode.appendChild(minPrijsText);
		maxPrijsNode.appendChild(maxPrijsText);
		//append cells to row
		prijsRij.appendChild(kledingTypeNode);
		prijsRij.appendChild(minPrijsNode);
		prijsRij.appendChild(maxPrijsNode);
	});
	} 
	//Als een merk geen sterren heeft, kunnen we geen prijs(tabel) weergeven! in de plaats daarvan gewoon de tekst "Geen sterren gevonden".
	else {
		var geenResultaatRij = tabel.insertRow(rijIndex);	
		geenResultaatRij.setAttribute("class", "priceRow");

		var legeResultaatNode1 = document.createElement("td")
		var geenResultaatNode = document.createElement("td")
		var legeResultaatNode2 = document.createElement("td");

		var geenResultaatText = document.createTextNode("Geen Sterren gevonden, prijs is onbepaald");
		geenResultaatNode.appendChild(geenResultaatText);

		geenResultaatRij.appendChild(legeResultaatNode1);
		geenResultaatRij.appendChild(geenResultaatNode);
		geenResultaatRij.appendChild(legeResultaatNode2);
	}
	//css klasse toevoegen aan de "prijslijstheaders", om deze via css een aparte opmaak te geven.
	document.querySelector("tr:nth-of-type(" + rijIndex + ")").className += " priceHeaderRow";
}